
- Only solution to the problem so far:
    - https://github.com/halfer/cd-demo-container/blob/master/.circleci/config.yml
    
    
- Other, non working attempts:
    - https://circleci.com/docs/2.0/workflows/#using-workspaces-to-share-data-among-jobs
    - https://github.com/circleci/circleci-docs/blob/master/.circleci/config.yml
    - https://discuss.circleci.com/t/workflows-with-jobs-triggered-by-tag/18543
    - https://discuss.circleci.com/t/workflow-job-with-tag-filter-being-run-for-every-commit/20762/3
    - https://discuss.circleci.com/t/circle-2-0-workflow-filters-should-be-logical-and-not-or/18231